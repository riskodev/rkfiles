#### Legal notice in English

This repository contains the composition files of the trademarks of Risko Software LTDA.  

The trademarks "Risko" and "Facilite" are registered with the brazilian property institute INPI.  

No copies or derivative works are permitted.

Copyright (c) 2021-2023 Risko - All rights reserved.

---

#### Aviso legal em Português

Este repositório contém os arquivos de composição das marcas comerciais da Risko Software LTDA.  

As marcas "Risko" e "Facilite" são registradas junto ao INPI.  

Nº de registro Risko: 912202190  
Nº de registro Facilite: 912201894  

Não sendo autorizadas cópias ou trabalhos derivados.

Copyright (c) 2021-2023 Risko - Todos os direitos reservados.

