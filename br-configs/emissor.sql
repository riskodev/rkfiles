CREATE TABLE avancnfes (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  operacao VARCHAR(240), 
  tipo VARCHAR(240), 
  serie DECIMAL(16,3),
  numnfe DECIMAL(16,3),
  codclientes INTEGER, 
  statusnfe VARCHAR(240), 
  datanfe DATE, 
  configs TEXT, 
  chaveacesso VARCHAR(240), 
  xmlnfe LONGBLOB,
  xmlorig LONGBLOB,
  xmlcancel LONGBLOB,
  motivocanc VARCHAR(240),
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE produtosavancnfes (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codavancnfes INTEGER,
  codprodutos INTEGER,
  codtributos INTEGER,
  dscr VARCHAR(240),
  qtd DECIMAL(16,3),
  valor DECIMAL(16,3),
  tot DECIMAL(16,3),
  totdesct DECIMAL(16,3),
  desct DECIMAL(16,3),
  vazio INTEGER
);
