
DELETE FROM caixa;
DELETE FROM contas;

DELETE FROM estoque;
UPDATE produtos SET qtdestoq=0;

DELETE FROM compras;
DELETE FROM produtoscompras;

DELETE FROM vendas;
DELETE FROM produtosvendas;

DELETE FROM ordens;
DELETE FROM servicosordens;

DELETE FROM nfes;

DELETE FROM nfes WHERE statusnfe LIKE "N%";
DELETE FROM nfes WHERE statusnfe LIKE "I%";
