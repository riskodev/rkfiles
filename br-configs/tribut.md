##### CSOSN 102 CFOP 5102 - Tributação Normal pelo Simples sem Destaque de ICMS

~~~
[ICMS]
CSOSN=102
Origem=0

[PIS]
CST=99

[COFINS]
CST=99
~~~

---

##### CSOSN 500 CFOP 5405 - Substituição Tributária pelo Simples

~~~
[ICMS]
CSOSN=500
Origem=0

[PIS]
CST=99

[COFINS]
CST=99
~~~

---

##### CSOSN 103 CFOP 5102 - Tributada pelo Simples com Parte de Isenção

~~~
[ICMS]
CSOSN=103
Origem=0

[PIS]
CST=99

[COFINS]
CST=99
~~~

##### CSOSN 101 CFOP 6102 - Venda Interestadual para Cliente Regime Normal

~~~
[ICMS]
CSOSN=101
Origem=0

[PIS]
CST=99

[COFINS]
CST=99
~~~

##### CSOSN 102 CFOP 6102 - Venda Interestadual para Cliente do Simples

~~~
[ICMS]
CSOSN=102
Origem=0

[PIS]
CST=99

[COFINS]
CST=99
~~~

##### CSOSN 102 CFOP 6108 - Venda Interestadual para Não Contribuinte

~~~
[ICMS]
CSOSN=102
Origem=0

[PIS]
CST=99

[COFINS]
CST=99
~~~

##### CSOSN 900 CFOP 5551 - Venda de Bem do Ativo Imobilizado

~~~
[ICMS]
CSOSN=900
Origem=0

[PIS]
CST=99

[COFINS]
CST=99
~~~