CREATE DATABASE facilite;
USE facilite;

CREATE TABLE municip (
   codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
   codmuni VARCHAR(240),
   municipio VARCHAR(240),
   uf VARCHAR(240),
   ufcod VARCHAR(240),
   estado VARCHAR(240)
);

CREATE TABLE emitentes (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  razaosocial VARCHAR(240), 
  nomefantasia VARCHAR(240), 
  cnpj VARCHAR(240), 
  regimeemi VARCHAR(240), 
  inscest VARCHAR(240), 
  inscmunic VARCHAR(240), 
  logradouro VARCHAR(240), 
  numero VARCHAR(240), 
  cep VARCHAR(240), 
  fone VARCHAR(240), 
  bairro VARCHAR(240), 
  uf VARCHAR(240), 
  cidade VARCHAR(240), 
  dados VARCHAR(240), 
  codcidade VARCHAR(240),
  complemento VARCHAR(240),
  crt VARCHAR(240),
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE clientes (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  nomerazao VARCHAR(240), 
  fone VARCHAR(240), 
  cpfcnpj VARCHAR(240), 
  inscest VARCHAR(240), 
  cep VARCHAR(240), 
  logradouro VARCHAR(240), 
  numero VARCHAR(240), 
  bairro VARCHAR(240), 
  uf VARCHAR(240), 
  cidade VARCHAR(240), 
  infoad TEXT, 
  codcidade VARCHAR(240),
  def INTEGER,
  atacado INTEGER,
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE caixa (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  descricaixa VARCHAR(240), 
  valor DECIMAL(16,3),
  tipo VARCHAR(240), 
  nomecaixa VARCHAR(240), 
  operador VARCHAR(240), 
  data DATE, 
  hora TIME, 
  valorreal DECIMAL(16,3),
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE contas (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  ocorrencia VARCHAR(240), 
  tipo VARCHAR(240), 
  status VARCHAR(240), 
  datavenc DATE, 
  valor DECIMAL(16,3),
  info TEXT, 
  datapag DATE,
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE crediario (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  codclientes INTEGER, 
  numcred DECIMAL(16,3),
  datavenc DATE, 
  status VARCHAR(240), 
  valor DECIMAL(16,3),
  outrosdados TEXT, 
  dataproc DATE,
  datapag DATE,
  numparcela INTEGER,
  totparcelas INTEGER,
  ini TEXT,
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE tributos (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  identifica VARCHAR(240), 
  cfop VARCHAR(240), 
  ini TEXT, 
  def INTEGER,
  regime VARCHAR(240),
  crt VARCHAR(240),
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE produtos (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  descricao VARCHAR(240), 
  valorunit DECIMAL(16,3),
  ean VARCHAR(240), 
  qtdestoq DECIMAL(16,3),
  minestoq DECIMAL(16,3),
  unidade VARCHAR(240), 
  categoria VARCHAR(240), 
  valoratacado DECIMAL(16,3),
  qtdatacado DECIMAL(16,3),
  valorcompra DECIMAL(16,3),
  ncm VARCHAR(240), 
  codtributos INTEGER, 
  dadosad TEXT, 
  codref VARCHAR(240),
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE estoque (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  codprodutos INTEGER, 
  tipo VARCHAR(240), 
  ocorrencia VARCHAR(240), 
  quantidade DECIMAL(16,3),
  data DATE, 
  hora TIME,
  qtdreal DECIMAL(16,3),
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE fornecs (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  nomerazao VARCHAR(240), 
  telefone VARCHAR(240), 
  cnpj VARCHAR(240), 
  dadosad TEXT, 
  contato VARCHAR(240),
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE compras (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  codfornecs INTEGER, 
  data DATE, 
  formapag VARCHAR(240), 
  valorbruto DECIMAL(16,3),
  valorfinal DECIMAL(16,3),
  chaveacesso VARCHAR(240),
  xmlnfe BLOB,
  tipopag VARCHAR(240),
  valorent DECIMAL(16,3),
  numparc INTEGER,
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE produtoscompras (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codcompras INTEGER,
  codprodutos INTEGER,
  dscr VARCHAR(240),
  qtd DECIMAL(16,3),
  valor DECIMAL(16,3),
  tot DECIMAL(16,3),
  totdesct DECIMAL(16,3),
  desct DECIMAL(16,3),
  vazio INTEGER
);

CREATE TABLE nfes (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  operacao VARCHAR(240), 
  statusnfe VARCHAR(240), 
  serie DECIMAL(16,3),
  numnfe DECIMAL(16,3),
  modelo VARCHAR(240), 
  datanfe DATE, 
  codvendas INTEGER, 
  chaveacesso VARCHAR(240), 
  dadosad TEXT, 
  xmlnfe LONGBLOB,
  xmlorig LONGBLOB,
  xmlcancel LONGBLOB,
  motivocanc VARCHAR(240),
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE vendas (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  tipo VARCHAR(240), 
  nomevend VARCHAR(240), 
  data DATE, 
  codclientes INTEGER, 
  formapag VARCHAR(240), 
  valorbruto DECIMAL(16,3),
  desconto DECIMAL(16,3),
  valorfinal DECIMAL(16,3),
  dadosad TEXT,
  hora TIME,
  nfe INTEGER,
  tipopag VARCHAR(240),
  valorent DECIMAL(16,3),
  numparc INTEGER,
  token VARCHAR(240),
  vazio INTEGER
);
CREATE TABLE produtosvendas (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codvendas INTEGER,
  codprodutos INTEGER,
  dscr VARCHAR(240),
  qtd DECIMAL(16,3),
  valor DECIMAL(16,3),
  tot DECIMAL(16,3),
  totdesct DECIMAL(16,3),
  desct DECIMAL(16,3),
  altura DECIMAL(16,3),
  largura DECIMAL(16,3),
  undm2 DECIMAL(16,3),
  vazio INTEGER
);

CREATE TABLE servicos (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  descri VARCHAR(240), 
  preco DECIMAL(16,3),
  valprest DECIMAL(16,3),
  dadosad TEXT, 
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE ordens (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  prestador VARCHAR(240), 
  dataini DATE, 
  datafim VARCHAR(240), 
  codclientes INTEGER, 
  info TEXT, 
  codvendas INTEGER, 
  status VARCHAR(240), 
  formapag VARCHAR(240), 
  valorfinal DECIMAL(16,3),
  totsoma DECIMAL(16,3),
  hora TIME,
  tipopag VARCHAR(240),
  valorent DECIMAL(16,3),
  numparc INTEGER,
  datefim DATE,
  token VARCHAR(240),
  vazio INTEGER
);

CREATE TABLE servicosordens (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codordens INTEGER,
  codservicos INTEGER,
  dscr VARCHAR(240),
  qtd DECIMAL(16,3),
  valor DECIMAL(16,3),
  tot DECIMAL(16,3),
  totdesct DECIMAL(16,3),
  desct DECIMAL(16,3),
  vazio INTEGER
);

CREATE TABLE globalcfgs (
  idcfg VARCHAR(240),
  cfg VARCHAR(240),
  decfg DECIMAL(16,3),
  txtcfg TEXT,
  filecfg BLOB
);

CREATE TABLE users (
  codigo INTEGER PRIMARY KEY AUTO_INCREMENT,
  codorig INTEGER,
  nomeorig VARCHAR(240),
  locker INTEGER,
  login VARCHAR(240),
  tipo VARCHAR(240),
  senha VARCHAR(240),
  estado VARCHAR(240),
  img LONGBLOB,
  img_ext VARCHAR(240),
  token VARCHAR(240),
  vazio INTEGER
);

INSERT INTO users (login, tipo, senha, estado)
VALUES ('Admin', 'Administrador', '21232f297a57a5a743894a0e4a801fc3', 'Liberado');

INSERT INTO users (login, tipo, senha, estado)
VALUES ('User', 'Usuário', 'ee11cbb19052e40b07aac0ca060c23ee', 'Liberado');

INSERT INTO globalcfgs (idcfg, cfg) VALUES ('localpag_cred', 'Na própria loja.');
INSERT INTO globalcfgs (idcfg, txtcfg) VALUES ('msg_cred', 'Após o vencimento haverá juros.');
INSERT INTO globalcfgs (idcfg, decfg) VALUES ('maxdesc_vista', '100');
INSERT INTO globalcfgs (idcfg, decfg) VALUES ('maxdesc_outros', '100');
INSERT INTO globalcfgs (idcfg, decfg) VALUES ('version_db', '0.904');
